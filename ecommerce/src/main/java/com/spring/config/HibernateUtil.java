package com.spring.config;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import com.spring.entities.CategoryClient;
import com.spring.entities.Client;
import com.spring.entities.ClientCategoryClient;
import com.spring.entities.Invoice;
import com.spring.entities.InvoiceDetail;
import com.spring.entities.Optionss;
import com.spring.entities.Page;
import com.spring.entities.Role;
import com.spring.entities.RoleOptions;
import com.spring.entities.Users;
import com.spring.entities.UsersRole;

public class HibernateUtil {
	
	private static SessionFactory sessionFactory;

	public static SessionFactory getSessionFactory() {

		if (sessionFactory == null) {

			try {

				Configuration configuration = new Configuration();

				// Hibernate settings equivalent to hibernate.cfg.xml's properties

				Properties settings = new Properties();

				settings.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");

				settings.put(Environment.URL, "jdbc:mysql://ubuntu-mysql.creativa.com:3306/comercio2?useSSL=false");

				settings.put(Environment.USER, "developer");

				settings.put(Environment.PASS, "rjniKzBeWObf");

				settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");

				settings.put(Environment.SHOW_SQL, "true");

				settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
				
				//settings.put(Environment.HBM2DDL_AUTO, "update");

				configuration.setProperties(settings);
				
				configuration.addAnnotatedClass(Users.class);
				configuration.addAnnotatedClass(Client.class);
				configuration.addAnnotatedClass(Optionss.class);
				configuration.addAnnotatedClass(Page.class);
				configuration.addAnnotatedClass(Role.class);
				configuration.addAnnotatedClass(RoleOptions.class);
				configuration.addAnnotatedClass(Invoice.class);
				configuration.addAnnotatedClass(InvoiceDetail.class);
				configuration.addAnnotatedClass(ClientCategoryClient.class);
				configuration.addAnnotatedClass(CategoryClient.class);
				configuration.addAnnotatedClass(UsersRole.class);

				ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();

				sessionFactory = configuration.buildSessionFactory(serviceRegistry);
				System.out.println("CONNECTION SUCCESSFUL ************************");
			} catch (Exception e) {

				e.printStackTrace();
				System.out.println("failed connection  ---------------------------");
			}

		}

		return sessionFactory;

	}

	public static void shutdown() {
		// Close caches and connection pools
		getSessionFactory().close();
	}
}
