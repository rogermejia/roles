package com.spring.service;

//import java.util.List;

public interface IGenericService {
	
	public Object saveObject(Object obj);

	public Object updateObject(Object obj);
	
	public String deleteObject(Object obj);
	
//	public List bringAll(Class clazz);
}
